#include <stdio.h>
#include <ctype.h>      // for isupper() etc.
#include <sys/socket.h> // for send() and recv()
#include <unistd.h>     // for sleep(), close()
#include <sstream>
#include <string.h>

#include "Auxiliary.h"
#include "HandleTCPClient.h"

#define RCVBUFSIZE 32 /* Size of receive buffer */

void HandleTCPClient(int clntSocket)
{
    std::string message;
    while (Read(clntSocket, message))
    {
        ConvertCase(message);
        Send(clntSocket, message);
    }

    close(clntSocket); /* Close client socket */
    info("close");
}

bool Read(int clntSocket, std::string &message)
{
    // 'clntSocket' is obtained from AcceptTCPConnection()
    char buffer[RCVBUFSIZE];
    int recvSize;
    bool received;
    std::stringstream ss;

    do
    {
        memset(&buffer, 0, RCVBUFSIZE);
        recvSize = recv(clntSocket, buffer, RCVBUFSIZE - 1, 0);
        if (buffer[0] != 0)
        {
            received = true;
            ss << buffer;
        }

    } while (recvSize == RCVBUFSIZE - 1);

    if (recvSize < 0)
    {
        DieWithError("recv() failed");
    }

    message = ss.str();
    info_s("Recv", message.c_str());
    return received;
}

void ConvertCase(std::string &text)
{
    for (std::string::size_type i = 0; i < text.size(); ++i)
    {
        text[i] = isupper(text[i]) ? tolower(text[i]) : toupper(text[i]);
    }
}

void Send(int clntSocket, const std::string &message)
{
    int size = message.length();

    /* Echo message back to client */
    if (send(clntSocket, message.c_str(), size, 0) != size)
    {
        DieWithError("send() failed");
    }

    info_s("Returned message", message.c_str());
}