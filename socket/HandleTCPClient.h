#ifndef _HANDLE_TCP_CLIENT_H_
#define _HANDLE_TCP_CLIENT_H_

#include <string>

extern void HandleTCPClient (int clntSocket);   /* TCP client handling function */
extern bool Read(int clntSocket, std::string& message);
extern void Send(int clntSocket, const std::string& message);
extern void ConvertCase(std::string& text);

#endif
