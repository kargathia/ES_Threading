#include <string>
#include <iostream>
#include <unistd.h>

#include "Auxiliary.h"
#include "AcceptTCPConnection.h"
#include "CreateTCPServerSocket.h"
#include "HandleTCPClient.h"

int main(int argc, char *argv[])
{
    int servSock; /* Socket descriptor for server */
    int clntSock; /* Socket descriptor for client */

    parse_args(argc, argv);

    servSock = CreateTCPServerSocket(argv_port);

    for (;;) /* Run forever */
    {
        clntSock = AcceptTCPConnection(servSock);
        std::string message;
        std::string line;
        while (Read(clntSock, message))
        {
            std::cout << "Reply? (or 'quit' to exit)" << std::endl;
            std::getline(std::cin, line);
            if (line == "quit")
            {
                close(clntSock);
                close(servSock);
                return 0;
            }
            Send(clntSock, line);
        }
    }
    /* NOT REACHED */
}
