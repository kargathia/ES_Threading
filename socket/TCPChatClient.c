#include <stdio.h>
#include <string.h> // for strlen()
#include <iostream>
#include <stdlib.h>     // for exit()
#include <sys/socket.h> // for send() and recv()
#include <unistd.h>     // for sleep(), close()
#include "HandleTCPClient.h"

#include "Auxiliary.h"
#include "CreateTCPClientSocket.h"

#define RCVBUFSIZE 32 /* Size of receive buffer */

int main(int argc, char *argv[])
{
    int sock;                    /* Socket descriptor */
    std::string receiveString;   /* String to send to echo server */

    parse_args(argc, argv);

    sock = CreateTCPClientSocket(argv_ip, argv_port);

    while (true)
    {
        std::cout << "Message? (or 'quit' to exit)" << std::endl;        
        getline(std::cin, receiveString);
        if (receiveString.compare("quit") == 0)
        {
            break;
        }

        Send(sock, receiveString);
        if (!Read(sock, receiveString))
        {
            break;
        }
    }

    close(sock);
    info("close & exit");
    exit(0);
}
