#include <pthread.h>
#include <string.h>
#include <unistd.h>

#include "Auxiliary.h"
#include "AcceptTCPConnection.h"
#include "CreateTCPServerSocket.h"
#include "HandleTCPClient.h"

static void *myThread(void *arg); /* thread that does the work */

struct threadArgs
{
    int socket;
    pthread_t threadID;
};

int main(int argc, char *argv[])
{
    int servSock;       /* Socket descriptor for server */
    int clntSock;       /* Socket descriptor for client */
    pthread_t threadID; /* Thread ID from pthread_create() */
    bool to_quit = false;

    parse_args(argc, argv);

    servSock = CreateTCPServerSocket(argv_port);

    while (!to_quit) /* run until someone indicates to quit... */
    {
        clntSock = AcceptTCPConnection(servSock);

        // DONE: create&start the thread myThread() te creeeren
        // use the POSIX operation pthread_create()
        //
        // make sure that clntSock and servSock are closed at the correct locations
        // (in particular: at those places where you don't need them any more)
        struct threadArgs args;
        args.socket = clntSock;
        args.threadID = threadID;

        int status = pthread_create(&threadID, NULL, myThread, &args);
        if (status != 0)
        {
            info_s("pthread_create() failed", strerror(status));
        }
    }

    close(servSock);
    // server stops...
    return (0);
}

static void *
myThread(void *threadArguments)
{
    // DONE: write the code to handle the client data
    // use operation HandleTCPClient()
    //
    // Hint: use the info(), info_d(), info_s() operations to trace what happens
    //
    // Note: a call of pthread_detach() is obligatory
    struct threadArgs *arguments = (struct threadArgs *)threadArguments;

    pthread_detach(arguments->threadID);
    HandleTCPClient(arguments->socket); 
    // Client socket is closed in HandleTCPClient

    return (NULL);
}
